# We put the sonar in a separate thread because it does not read quite so fast in the data acquisition thread and we need to read faster.

from dataClass import dataClass
from multiprocessing import Queue
from PyQt5 import QtGui, QtCore

import config
import globals
import time
import sys

# Sensor Classes

try:
    from sonar import sonar
except Exception as e:
    print(e)


class rfidThread(QtCore.QThread):
    errorSignal = QtCore.pyqtSignal(str)
    updateSignal = QtCore.pyqtSignal(str)

    def __init__(self):
        QtCore.QThread.__init__(self)
        self.start()

    def __del__(self):
        self.wait()

    def run(self):
        print("Starting Sonar Thread...")

        # Initialize
        try:
            utrasonic = sonar()
        except:
            self.errorSignal.emit("Sonar failed to start.")
        # Get Sensor Readings
        while 1:
            startTime = time.time()

            try:
                globals.distanceSonar = ultrasonic.measure() # Measure ultrasonic
            except NameError:
                self.errorSignal.emit("Sonar not initialized.")
            except KeyboardInterrupt:
                quit()
            except:
                self.errorSignal.emit("Failed to read Sonar.")


            time.sleep(config.SONAR_UPDATE_PERIOD + startTime - time.time())
