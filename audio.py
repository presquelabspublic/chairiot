# This class controls the audio on the Pi primarily using the Pygame library

try:
    import pygame
except Exception as e:
    print(e)


# We use a USB audio speaker to make noises when things happen. The USB audio speaker uses the pygame library to run certain functions.
class audio():

    # No need to create an instance of the class. Just call this function from wherever, as long as you import it.
    @staticmethod
    def play_audio_file(file):
        try:
            pygame.mixer.init() # Initialize the mixer
            pygame.mixer.music.set_volume(0.5) # Set the volume to half
            pygame.mixer.music.load(file) # Load the WAV (must be WAV) file that you want to play. File must be in the same folder
            pygame.mixer.music.play() # Play the file on the speaker
        except Exception as e:
            return e
