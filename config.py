# This script defines the hardware configuration of the Pi

import sys

ADC_VOLTAGE =    0  # Pin 0 on the ADC
ADC_CURRENT =    1  # Pin 1 on the ADC
ADC_FSR     =    2  # Pin 2 on the ADC
GPS_TX      =    14 # Pin 8 on the PI
GPS_RX      =    15 # Pin 10 on the PI
SONAR_TRIG  =    17 # Pin 11 on the PI
SONAR_ECHO  =    27 # Pin 13 on the PI
RFID_MOSI   =    10 # Pin 19 on the PI
RFID_MISO   =    9  # Pin 21 on the PI
RFID_SCLK   =    11 # Pin 23 on the PI
RFID_CS     =    8  # Pin 24 on the PI
ADC_MOSI    =    10 # Pin 19 on the PI
ADC_MISO    =    9  # Pin 21 on the PI
ADC_SCLK    =    11 # Pin 23 on the PI
ADC_CS      =    7  # Pin 26 on the PI
TEMPE_CART  =    16 # Pin 38 on the PI
SERVO_PIN   =    25 # Pin 22 on the PI
HX711_CLK1  =    26 # Pin 37 on the PI
HX711_DAT1  =    19 # Pin 35 on the PI
HX711_CLK2  =    24 # Pin 18 on the PI
HX711_DAT2  =    23 # Pin 16 on the PI
HX711_CLK3  =    5  # Pin 29 on the PI
HX711_DAT3  =    6  # Pin 31 on the PI
HX711_CLK4  =    12 # Pin 32 on the PI
HX711_DAT4  =    13 # Pin 33 on the PI
LOCK_PULSE  =    50 # Pulse for the Servo, which is controlled by PWM
UNLOCK_PULSE=    250 # Pulse for the Servo, which is controlled by PWM

# Passcode text that shows on startup
PASSCODE_DEFAULT_TEXT = "Enter Your Passcode"

# Text on the Errors button
ERRORS_BUTTON_TEXT = "Errors: "

# Unlock passcodes
PASSCODES = ["12345", "666"]

UPDATE_PERIOD = 5 # How often the DAQ thread updates, seconds
RFID_UPDATE_PERIOD = .5 # How often the RFID reader reads, seconds
GPS_UPDATE_PERIOD = 10 # How often the GPS reads, seconds
SONAR_UPDATE_PERIOD = .5 # How often the Sonar reads, seconds

PROMPT_DURATION = 1 # If there is an error, the prompt stays up for this amount of time in seconds

fsrThreshold = 100 # Define the value that the FSR thinks there is or is not a force acting on it.

SERVER_URL = "http://chairiot.presquelabs.com/add.php" # This is the server that the Pi currently sends data to (see add_secure.php)
