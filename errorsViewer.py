# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'errorsViewer.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_errorsViewer(object):
    def setupUi(self, errorsViewer):
        errorsViewer.setObjectName("errorsViewer")
        errorsViewer.resize(584, 326)
        errorsViewer.setStyleSheet("background-color: black;\n"
"border: 1px solid white;\n"
"color: white;\n"
"font: 12pt \"Bahnschrift\";")
        self.verticalLayout = QtWidgets.QVBoxLayout(errorsViewer)
        self.verticalLayout.setObjectName("verticalLayout")
        self.scrollArea = QtWidgets.QScrollArea(errorsViewer)
        self.scrollArea.setStyleSheet("border: 1px solid black;")
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 564, 279))
        self.scrollAreaWidgetContents.setStyleSheet("")
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.errorText = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.errorText.sizePolicy().hasHeightForWidth())
        self.errorText.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Bahnschrift")
        font.setPointSize(12)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.errorText.setFont(font)
        self.errorText.setStyleSheet("")
        self.errorText.setAlignment(QtCore.Qt.AlignCenter)
        self.errorText.setObjectName("errorText")
        self.verticalLayout_2.addWidget(self.errorText)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.verticalLayout.addWidget(self.scrollArea)
        self.closePushButton = QtWidgets.QPushButton(errorsViewer)
        self.closePushButton.setStyleSheet("border: 1px solid grey;")
        self.closePushButton.setObjectName("closePushButton")
        self.verticalLayout.addWidget(self.closePushButton)

        self.retranslateUi(errorsViewer)
        QtCore.QMetaObject.connectSlotsByName(errorsViewer)

    def retranslateUi(self, errorsViewer):
        _translate = QtCore.QCoreApplication.translate
        errorsViewer.setWindowTitle(_translate("errorsViewer", "Dialog"))
        self.errorText.setText(_translate("errorsViewer", "Hello this is longer now"))
        self.closePushButton.setText(_translate("errorsViewer", "Close"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    errorsViewer = QtWidgets.QDialog()
    ui = Ui_errorsViewer()
    ui.setupUi(errorsViewer)
    errorsViewer.show()
    sys.exit(app.exec_())

