# This class is primarily for passive RFID reading.

from dataClass import dataClass
from dataTable import dataTable
from multiprocessing import Queue
from PyQt5 import QtGui, QtCore

import config
import globals
import time
import sys

# Sensor Classes

try:
    from rfid import rfid
except Exception as e:
    print(e)

class rfidThread(QtCore.QThread):
    errorSignal = QtCore.pyqtSignal(str)
    cardScannedSignal = QtCore.pyqtSignal(str)

    def __init__(self):
        QtCore.QThread.__init__(self)
        self.start()

    def __del__(self):
        self.wait()

    def run(self):
        print("Starting RFID Thread...")

        try:
            # Initialize
            rfidReader = rfid()
        except:
            self.errorSignal.emit("RFID failed to start.")

        try:
            # Create a new database connection
            newDatabase = dataTable("blackbox")
        except:
            self.errorSignal.emit("Failed to initialize data collection.")

        # Get Sensor Readings
        while 1:
            startTime = time.time()

            try:
                uid = rfidReader.read_rfid()
                if uid is not None:
                    globals.rfidReading = uid
                    startTime = startTime + 1
                    if uid == newDatabase.checkUsers(uid, "password"):
                        # Check user database for user id and password
                        print("Access granted to User : " + uid)
                        self.cardScannedSignal.emit("granted")
                    else:
                        print("Access denied to User: " + uid)
                        self.cardScannedSignal.emit("denied")
                        globals.rfidReading = 0
            except NameError:
                self.errorSignal.emit("RFID Reader not initialized.")
            except KeyboardInterrupt:
                quit()
            except Exception as e:
                print(e)
                self.errorSignal.emit("Failed to read RFID.")

            if (time.time() - startTime < config.RFID_UPDATE_PERIOD):
                time.sleep(config.RFID_UPDATE_PERIOD + startTime - time.time())
