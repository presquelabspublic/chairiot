# This class controls the servo

import time
import sys
import pigpio
import os

class servo():

    def __init__(self, pin, number, mini, maxi, direction):
        self.direction = direction  # What direction the servo rotates in
        self.max = maxi             # Maximum rotation
        self.min = mini             # Minimum rotation
        self.number = number        # How much to rotate
        self.pin = pin              # Which pin is the servo controlled by

        self.angle = 0
        self.pwm = float(maxi - mini)/2
        self.per = 50

        self.pi = pigpio.pi()
        self.pi.set_PWM_frequency(pin, 50)
        self.moveAngle(0)

    # takes -90 to 90
    def moveAngle(self, angle):
        # Move the servo by a specified angle
        self.angle = angle
        # PWM signal to move the servo
        self.pwm = self.min + ((self.max - self.min) * (float(angle + 90)/ 180))
        if (self.direction == "right"):
            self.per = 100 - float(angle + 90) / 1.8
        else:
            self.per = float(angle + 90) / 1.8
        print("Duty Cycle = " + str(self.pwm))
        self.move(self.pwm)

    def movePWM(self, pwm):
        # Move the servo a specified PWM
        self.angle = float(pwm - self.min) / (self.max - self.min) * 180 - 90
        self.per = float(pwm - self.min) / (self.max - self.min) * 100
        self.move(self.pwm)

    def move(self, pwm):
        self.pi.set_PWM_dutycycle(self.pin, pwm)

    def movePer(self, per):
        self.angle = float(180 * per / 100) - 90
        self.pwm = float(self.max - self.min) * per / 100 + self.min
        self.per = per
        self.move(self.pwm)
