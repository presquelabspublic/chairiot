# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'prompt.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_prompt(object):
    def setupUi(self, prompt):
        prompt.setObjectName("prompt")
        prompt.resize(276, 167)
        prompt.setStyleSheet("background-color: black;\n"
"border: 1px solid white;\n"
"")
        self.horizontalLayout = QtWidgets.QHBoxLayout(prompt)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.promptText = QtWidgets.QLabel(prompt)
        font = QtGui.QFont()
        font.setFamily("Bahnschrift")
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.promptText.setFont(font)
        self.promptText.setStyleSheet("font: 14pt \"Bahnschrift\";\n"
"color: white;\n"
"border: 1px solid black;")
        self.promptText.setAlignment(QtCore.Qt.AlignCenter)
        self.promptText.setObjectName("promptText")
        self.horizontalLayout.addWidget(self.promptText)

        self.retranslateUi(prompt)
        QtCore.QMetaObject.connectSlotsByName(prompt)

    def retranslateUi(self, prompt):
        _translate = QtCore.QCoreApplication.translate
        prompt.setWindowTitle(_translate("prompt", "Dialog"))
        self.promptText.setText(_translate("prompt", "Prompt Text"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    prompt = QtWidgets.QDialog()
    ui = Ui_prompt()
    ui.setupUi(prompt)
    prompt.show()
    sys.exit(app.exec_())

