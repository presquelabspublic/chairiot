# dataTable is a class that creates the on-board non-volatile SQL database

import sqlite3
import datetime
import time
from dataClass import dataClass

class dataTable():

    # fileName is the name of the database that we want to create, or access if it's already created
    def __init__(self, fileName):
        self.database = sqlite3.connect(fileName) # Connect to the database
        self.cursor = self.database.cursor() # Create a cursor object

        # Cursor executes the CREATE TABLE code to create a table called data
        # This code defines the variable names and their datatypes
        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS data(
                TS FLOAT,
                ID TEXT,
                LATITUDE DECIMAL,
                LONGITUDE DECIMAL,
                BATT_VOLTAGE DECIMAL,
                BATT_CURRENT DECIMAL,
                BATT_TEMP DECIMAL,
                CAVITY_TEMP DECIMAL,
                LOAD DECIMAL,
                RFID DECIMAL,
                SONAR DECIMAL,
                FORCEDENTRYDETECTED BOOLEAN,
                PRIMARY KEY (TS, ID)
            )''')

        # Cursor also creates a new table of USERS and their PASSWORDS. The table is called users
        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS users(
                UID TEXT PRIMARY KEY,
                PASSWORD TEXT
            )''')
        self.database.commit()

    # Populate the data table with these inputs
    def populateData(self, ts, id, latitude, longitude, battVoltage, battCurrent, battTemp, cavityTemp, load, rfid, sonar, forcedEntryDetected):
        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        # Execute SQL code
        self.cursor.execute('''
            INSERT INTO data(TS, ID, LATITUDE, LONGITUDE, BATT_VOLTAGE, BATT_CURRENT, BATT_TEMP, CAVITY_TEMP, LOAD, RFID, SONAR, FORCEDENTRYDETECTED)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''', (timestamp, id, latitude, longitude, battVoltage, battCurrent, battTemp, cavityTemp, load, rfid, sonar, forcedEntryDetected))

        # Save database
        self.database.commit()

    # Populate the users table with these inputs
    def populateUsers(self, uid, password):
        # Execute SQL code
        self.cursor.execute('''
            INSERT INTO users(UID, PASSWORD)
            VALUES(?,?)''', (uid, password))

        # Save database
        self.database.commit()

    # This function checks if users exist in the user table
    def checkUsers(self, uid, password):
        # Execute the SQL code to select user
        self.cursor.execute('''
            SELECT UID FROM users WHERE UID=? AND PASSWORD=?''', (uid, password))
        try:
            user = self.cursor.fetchone()[0]
        except Exception as e:
            user = "User not found."
        return user

    # This function populates the data table with a dataClass instance
    def populateFromClass(self, currentDataClass, index):
        timestamp = datetime.datetime.fromtimestamp(currentDataClass.ts[index]).strftime('%Y-%m-%d %H:%M:%S')
        self.cursor.execute('''
            INSERT INTO data(TS, ID, LATITUDE, LONGITUDE, BATT_VOLTAGE, BATT_CURRENT, BATT_TEMP, CAVITY_TEMP, LOAD, RFID, SONAR, FORCEDENTRYDETECTED)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
                (timestamp,
                currentDataClass.id[index],
                currentDataClass.latitude[index],
                currentDataClass.longitude[index],
                currentDataClass.battVoltage[index],
                currentDataClass.battCurrent[index],
                currentDataClass.battTemp[index],
                currentDataClass.cavityTemp[index],
                currentDataClass.load[index],
                currentDataClass.rfid[index],
                currentDataClass.sonar[index],
                currentDataClass.forcedEntryDetected[index]))
        self.database.commit()

    # This function returns the last numPoints recent points
    def getRecentPoints(self, numPoints):
        self.cursor.execute('''SELECT * FROM data''')
        allRows = self.cursor.fetchall()
        outData = dataClass()
        count = 0
        for row in allRows:
            if (count == numPoints):
                break
            outData.addPoint(
                row[0],
                row[1],
                row[2],
                row[3],
                row[4],
                row[5],
                row[6],
                row[7],
                row[8],
                row[9],
                row[10],
                row[11]
            )
            count = count + 1
        return outData
