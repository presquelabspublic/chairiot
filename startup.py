# Main launch point into the entire program. Startup calls MainWindowController

import sys
import globals
from PyQt5 import QtCore, QtGui, QtWidgets
from mainWindowController import *

#Function that runs on startup
def main():
    try:

        # Init the globals
        globals.init()

        # Launch the main UI window from mainWindow.py
        app = QApplication(sys.argv)
        MainWindowForm = MainWindowController(app)
        #MainWindowForm.show()
        MainWindowForm.showFullScreen()
        # the window would exit immediately without this
        sys.exit(app.exec_())

    finally:
        print("Has Quit")

# Runs main on start
if __name__ == "__main__":
    main()
