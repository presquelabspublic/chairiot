# This class is a volatile form of data storage. It is an object with properties that can be written to a database

import sys
import time

class dataClass():

    # This class has the following properties that we initialize (in arrays)
    def __init__(self):
        self.ts = [] # Timestamp
        self.id = [] # Identification
        self.latitude = []
        self.longitude = []
        self.battVoltage = []
        self.battCurrent = []
        self.battTemp = []
        self.cavityTemp = []
        self.load = []
        self.rfid = []
        self.sonar = []
        self.forcedEntryDetected = []

    # Add a new datapoint from another dataClass instance
    # newPoint is another dataClass instance
    # index is the index of the dataClass that we want to access
    def addPointFromClass(self, newPoint, index):
        self.ts.append(newPoint.ts[index])
        self.id.append(newPoint.id[index])
        self.latitude.append(newPoint.latitude[index])
        self.longitude.append(newPoint.longitude[index])
        self.battVoltage.append(newPoint.battVoltage[index])
        self.battCurrent.append(newPoint.battCurrent[index])
        self.battTemp.append(newPoint.battTemp[index])
        self.cavityTemp.append(newPoint.cavityTemp[index])
        self.load.append(newPoint.load[index])
        self.rfid.append(newPoint.rfid[index])
        self.sonar.append(newPoint.sonar[index])
        self.forcedEntryDetected.append(newPoint.forcedEntryDetected[index])

    # Add a new datapoint in code
    def addPoint(self, ts, id, latitude, longitude, battVoltage, battCurrent, battTemp, cavityTemp, load, rfid, sonar, forcedEntryDetected):
        self.ts.append(ts)
        self.id.append(id)
        self.latitude.append(latitude)
        self.longitude.append(longitude)
        self.battVoltage.append(battVoltage)
        self.battCurrent.append(battCurrent)
        self.battTemp.append(battTemp)
        self.cavityTemp.append(cavityTemp)
        self.load.append(load)
        self.rfid.append(rfid)
        self.sonar.append(sonar)
        self.forcedEntryDetected.append(forcedEntryDetected)
