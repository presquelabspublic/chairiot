# The PN532 RFID breakout board from Adafruit can be detailed here: https://learn.adafruit.com/adafruit-nfc-rfid-on-raspberry-pi/overview. This script makes use of two Adafruit libraries - GPIO and PN532self.

# The PN532-Raspberry Pi library can be found here: https://github.com/adafruit/Adafruit_Python_PN532. This library is reproduced in this directory.

# The Adafruit GPIO library can be found here: https://github.com/adafruit/Adafruit_Python_GPIO. This library is also reproduced in this directory

# Detecting and reading a block from a MiFare NFC card.
# Original Author: Tony DiCola for Adafruit Industries
# Modified for Presque Labs LLC

import binascii
import sys
import config
import Adafruit_PN532 as PN532  # PN532 iibrary
import Adafruit_GPIO.SPI as SPI # Adafruit GPIO library
import RPi.GPIO as GPIO

class rfid():

    def __init__(self):

        # Create an instance of the PN532 class.

        # Software SPI Configuration
        self.pn532 = PN532.PN532(cs=config.RFID_CS, sclk=config.RFID_SCLK, mosi=config.RFID_MOSI, miso=config.RFID_MISO)

        # Call begin to initialize communication with the PN532.  Must be done before any other calls to the PN532!
        self.pn532.begin()

        # Configure PN532 to communicate with MiFare cards.
        self.pn532.SAM_configuration()

    def read_rfid(self):
        # Check if a card is available to read.
        uid = self.pn532.read_passive_target()  # a PN532 function
        if uid is not None:
            uid = str(binascii.hexlify(uid))
            return uid
            # uid must be formatted .format(binascii.hexlify(uid))
        else:
            return None
