import sys

# Qt imports
import PyQt5
from PyQt5 import QtCore
from PyQt5.QtWidgets import *

import datetime
import time
import globals
import config

# window from QT Creator generated with: "pyuic5 mainwindow.ui -x mainWindow.py"
import errorsViewer

#from spImuGraphsController import *

#create class for the GUI
class errorsViewerController(QDialog, errorsViewer.Ui_errorsViewer):

	#access variables inside of the UI's File
    def __init__(self, errorsPushButton):
        super(self.__class__, self).__init__()
        #defined in UI File
        self.setupUi(self)
        self.errorText.setText(globals.errorText)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.errorsPushButton = errorsPushButton
        self.closePushButton.clicked.connect(lambda:self.closePushButtonClicked())

    # This function defines what happens when the Close button is clicked
    def closePushButtonClicked(self):
        self.close() # Close this class
        globals.unreadErrors = 0 # Set Errors to 0
        self.errorsPushButton.setText("Errors: " + str(globals.unreadErrors)) # Change the button text
        globals.errorText = ""
        globals.showingErros = False

    # Update the GUI text
    def updateText(self):
        self.errorText.setText(globals.errorText)
