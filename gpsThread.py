# The GPS is on a separate thread because it tends to stop everything until it finds a valid GPS signal. The gpsThread class reads from the GPS and updates the values that it receives

################################################################################
##### IMPORT PYTHON CLASSES ####################################################
################################################################################

from dataClass import dataClass
from multiprocessing import Queue
from PyQt5 import QtGui, QtCore

import config
import globals
import time
import sys

################################################################################
##### Import Sensor Classes ####################################################
################################################################################

try:
    from pa6h import pa6h
except Exception as e:
    print(e)

################################################################################
##### CLASS ####################################################################
################################################################################

# This class starts the GPS and reads from it
class gpsThread(QtCore.QThread):
    errorSignal = QtCore.pyqtSignal(str)

    # Initialize the thread
    def __init__(self):
        QtCore.QThread.__init__(self)
        self.start()

    # Delete the thread
    def __del__(self):
        self.wait()

    # Code that runs once the thread is initialized
    def run(self):
        print("Starting GPS Thread... \n")

        ########################################################################
        ##### Initialize sensors ###############################################
        try:
            gps = pa6h()
        except:
            self.errorSignal.emit("Could not read GPS.")

        ########################################################################
        ##### LOOP TO UPDATE SENSORS/STORE VALUES ##############################
        ########################################################################

        while 1:
            startTime = time.time()

            try:
                startTime = time.time()
                # Update all of the sensors and put their values in globals
                globals.latitude = gps.read_gps()[0]
                globals.longitude = gps.read_gps()[1]
            except NameError:
                self.errorSignal.emit("GPS module not initialized.")
            except Exception:
                self.errorSignal.emit("Failed to read GPS.")

            time.sleep(config.UPDATE_PERIOD + startTime - time.time())
