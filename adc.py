# The MCP3008 is a commonly used Analog-to-Digital Converter. It has 8 channels, but we only use 3 - for the voltage sensor, the current sensor, and the force-sensitive resistor, which all return analog values. This script takes advantage of the already existing MCP3008 library written for Python by Adafruit. The MCP3008 library is duplicated in this directory but can also be found here: https://github.com/adafruit/Adafruit_Python_MCP3008

# Original Author: Tony DiCola for Adafruit
# License: Public Domain
# Modified for use by Presque Labs LLC
import time
import config

# Import SPI library (for hardware SPI) and MCP3008 library.
import Adafruit_GPIO.SPI as SPI # Adafruit Library for SPI
import Adafruit_MCP3008 as MCP3008 # Adafruit Library for MCP3008 ADC

class adc():

    # Create an instance of the MCP3008 class
    def __init__(self):

        # Software SPI Configuration
        # This ADC is bit-banged. Instead of using hardware pins, we use normal digital pins to simulate the timing of the software. We need to supply it with the clock, chip select, MISO, and MOSI pins, defined in CONFIG.
        self.mcp = MCP3008.MCP3008(clk=config.ADC_SCLK, cs=config.ADC_CS, miso=config.ADC_MISO, mosi=config.ADC_MOSI)

    # Read general value from the ADC
    def read_adc(self, channel):
        # This is an Adafruit function that reads a value from the ADC
        channelValue = self.mcp.read_adc(channel)
        return channelValue

    # Read voltage value from voltage sensor
    def read_voltage(self):
        # Calls read_adc and specifies the voltage pin
        channelValue = self.read_adc(config.ADC_VOLTAGE)
        # The value from the ADC is referenced against the AREF value on the ADC, which is 3.3V. It is also referenced against the voltage sensor maximum value, which is 5V. Finally, since the max value is 1024, we divide it by 1023.
        voltage = channelValue * 3.3 * 5 / 1023
        return voltage

    # Read current from the current sensor
    def read_current(self):
        # Calls read_adc and specifies the current pin
        channelValue = self.read_adc(config.ADC_CURRENT)
        # The value from the ADC is referenced against the AREF value on the ADC, which is 3.3V. It is also referenced against the current sensor maximum value.
        current = channelValue * 3.3 * 5 / 1023
        return current

    # Read resistance from the force-sensitive resistor
    def read_fsr(self):
        # Calls read_adc and specifies the resistor pin
        channelValue = self.read_adc(config.ADC_FSR)
        # We take the straight resistance value from the FSR.
        fsr = channelValue
        return fsr
