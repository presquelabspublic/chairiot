# The daqThread is the class that does primary data acquisition. It is the class which checks most of the sensors and makes calls to the mainWindowController to update the GUI.

################################################################################
##### IMPORT PYTHON CLASSES ####################################################
################################################################################

from dataClass import dataClass
from dataTable import dataTable
from serverCom import serverCom
from multiprocessing import Queue
from PyQt5 import QtGui, QtCore

import config
import globals
import datetime
import time
import sys

################################################################################
##### Import Sensor Classes ####################################################
################################################################################

try:
    from adc import adc
except Exception as e:
    print(e)

try:
    from pa6h import pa6h
except Exception as e:
    print(e)

try:
    from rfid import rfid
except Exception as e:
    print(e)

try:
    from thermistor import thermistor
except Exception as e:
    print(e)

try:
    from thermistorI2C import thermistorI2C
except Exception as e:
    print(e)

try:
    from hx711 import hx711
except Exception as e:
    print(e)

try:
    from sonar import sonar
except Exception as e:
    print(e)

################################################################################
##### CLASS ####################################################################
################################################################################


# The DAQ thread runs in the background collecting data. It initializes all the sensors and attempts to read from them, and it throws an error if none of that ends up happening
class daqThread(QtCore.QThread):

    # PyQT Signals to prompt the GUI to do something
    updateGuiSignal = QtCore.pyqtSignal(str)
    errorSignal = QtCore.pyqtSignal(str)
    updateSonarGuiSignal = QtCore.pyqtSignal(str)

    # Initialize the thread
    def __init__(self):
        QtCore.QThread.__init__(self)
        self.start()

    # Delete the thread
    def __del__(self):
        self.wait()

    # Code that runs once the thread is initialized
    def run(self):
        print("Starting DAQ Thread... \n")

        # Initialize structure for storing data locally
        newData = dataClass() # Volatile memory that stores the data
        newDatabase = dataTable("blackbox") # Non-volatile database where memory is stored periodically
        server = serverCom() # Non-volatile database hosted on a server somewhere.

        ########################################################################
        ##### INITIALIZE ALL SENSORS ###########################################
        ########################################################################
        try:
            self.sonar = sonar() # Create a sonar instance
        except:
            self.errorSignal.emit("Sonar failed to start.")

        try:
            analogDigitalConverter = adc() # Create an ADC instance
        except:
            self.errorSignal.emit("ADC failed to start.")

        try:
            thermocouple1 = thermistor(0) # Create an instance for the 1st thermistor
        except:
            self.errorSignal.emit("Temperature Sensor 1 failed to start.")

        try:
            thermocouple2 = thermistor(1) # Create an instance for the second thermistor
        except:
            self.errorSignal.emit("Temperature Sensor 2 failed to start.")

        try:
            thermoBatt = thermistorI2C() # Create an instance for the thermistor on the battery
        except:
            self.errorSignal.emit("Battery temperature sensor failed to start.")

        try:
            load1 = hx711(config.HX711_DAT1, config.HX711_CLK1) # Create an instance of the strain gauge / amplifier class
        except:
            self.errorSignal.emit("Load Sensor 1 failed to start.")

        try:
            load2 = hx711(config.HX711_DAT2, config.HX711_CLK2) # Create an instance of the strain gauge / amplifier class
        except:
            self.errorSignal.emit("Load sensor 2 failed to start.")

        try:
            load3 = hx711(config.HX711_DAT3, config.HX711_CLK3) # Create an instance of the strain gauge / amplifier class
        except:

            self.errorSignal.emit("Load Sensor 3 failed to start.")

        try:
            load4 = hx711(config.HX711_DAT4, config.HX711_CLK4) # Create an instance of the strain gauge / amplifier class
        except:
            self.errorSignal.emit("Load Sensor 4 failed to start.")

        try:
            server = serverCom() # Create an instance for the server communication.
        except:
            self.errorSignal.emit("Failed to initialize Server Communication class.")

        ########################################################################
        ##### LOOP TO UPDATE SENSORS/STORE VALUES ##############################
        ########################################################################

        while 1:
            startTime = time.time()

            try:
                globals.distanceSonar = self.sonar.measure() # Measure the sonar value and store it in a variable in globals
            except NameError:
                self.errorSignal.emit("Sonar not initialized.") # A NameError indicates that the instance was unable to start
            except:
                self.errorSignal.emit("Failed to read sonar.")
            self.updateSonarGuiSignal.emit("update") # Update the GUI

            try:
                globals.fsrReading = analogDigitalConverter.read_fsr() # Read the force-sensitive resistor from the ADC
                if globals.fsrReading > config.fsrThreshold: # If the fsrReading is greater than the force threshold, then
                    globals.doorState = True # Notify that the door is locked
                    if globals.lockState: # If the FSR has pressure and the door is locked, then
                        globals.doorClosedSinceLock = True # The door is closed
                else:
                    if not globals.lockState: # If the FSR has pressure and the door is not locked then
                        globals.doorClosedSinceLock = False # The door is open
                    globals.doorState = False # Notify that the door is unlocked
            except NameError:
                self.errorSignal.emit("Analog-Digital Converter not initialized.")
            except:
                self.errorSignal.emit("Failed to read force sensitive resistor.")

            try:
                globals.distanceSonar = self.sonar.measure() # Measure the sonar value
            except NameError:
                self.errorSignal.emit("Sonar not initialized.")
            except:
                self.errorSignal.emit("Failed to read sonar.")
            self.updateSonarGuiSignal.emit("update") # Update the GUI

            try:
                globals.voltageBattery = analogDigitalConverter.read_voltage() # Measure the voltage value
                globals.percentageBattery = round(((globals.voltageBattery - 10.5) / (12.8 - 10.5) * 100),0) # Set the battery percentage (voltageReading - Minimum) / (Maximum - Minimum)
            except NameError:
                self.errorSignal.emit("Analog-Digital Converter not initialized.")
            except:
                self.errorSignal.emit("Failed to read voltage sensor.")

            try:
                globals.distanceSonar = self.sonar.measure() # Measure the sonar value
            except NameError:
                self.errorSignal.emit("Sonar not initialized.")
            except:
                self.errorSignal.emit("Failed to read sonar.")
            self.updateSonarGuiSignal.emit("update") # Update the GUI

            try:
                globals.currentBattery = analogDigitalConverter.read_current() # Read the current sensor
            except NameError:
                self.errorSignal.emit("Analog-Digital Converter not initialized.")

            try:
                globals.distanceSonar = self.sonar.measure() # Measure the sonar value
            except NameError:
                self.errorSignal.emit("Sonar not initialized.")
            except:
                self.errorSignal.emit("Failed to read sonar.")
            self.updateSonarGuiSignal.emit("update") # Update the GUI

            try:
                globals.tempBattery = thermoBatt.readTemperature(units='F') # Measure the temperature of the battery
            except NameError:
                self.errorSignal.emit("Battery temperature sensor not initialized.")
            except:
                self.errorSignal.emit("Failed to read battery temperature sensor.")

            try:
                globals.distanceSonar = self.sonar.measure() # Measure the sonar value
            except NameError:
                self.errorSignal.emit("Sonar not initialized.")
            except:
                self.errorSignal.emit("Failed to read sonar.")
            self.updateSonarGuiSignal.emit("update") # Update the GUI

            try:
                globals.tempCabin = (thermocouple1.read_temp()[1] + thermocouple2.read_temp()[1]) / 2 # Read the temperature of the cabin (2 sensors)
            except NameError:
                self.errorSignal.emit("Cavity temperature sensors not initialized.")
            except:
                self.errorSignal.emit("Failed to read cavity temperature sensors.")

            try:
                globals.distanceSonar = self.sonar.measure() # Measure the sonar value
            except NameError:
                self.errorSignal.emit("Sonar not initialized.")
            except:
                self.errorSignal.emit("Failed to read sonar.")
            self.updateSonarGuiSignal.emit("update") # Update the GUI

            try:
                globals.weightLoad = load1.get_weight(5) # Measure the weight on top of the strain gage
            except NameError:
                self.errorSignal.emit("Load sensors not initialized.")
            except:
                self.errorSignal.emit("Failed to read load sensors.")

            try:
                globals.distanceSonar = self.sonar.measure() # Measure the sonar value
            except NameError:
                self.errorSignal.emit("Sonar not initialized.")
            except:
                self.errorSignal.emit("Failed to read sonar.")

            try:
                newData.addPoint(time.time(), globals.id, globals.latitude, globals.longitude, globals.voltageBattery, globals.currentBattery, globals.tempBattery, globals.tempCabin, globals.weightLoad, globals.rfidReading, globals.distanceSonar, globals.forcedEntryDetected) # Add a new data point to the volatile memory that stores data

                newDatabase.populateFromClass(newData, -1) # Add a new data point to non-volatile memory
            except Exception as e:
                print(e)
                self.errorSignal.emit("Failed to write to local database.")

            self.updateGuiSignal.emit("update") # Update the GUI

            try:
                server.postDataClass(newData, -1) # Send data to the server
            except Exception as e:
                print(e)
                self.errorSignal.emit("Failed to write to server.")

            while (time.time() - startTime < config.UPDATE_PERIOD): # Run this code for as long as the update period is
                try:
                    globals.distanceSonar = self.sonar.measure() # Measure the sonar. Measure until time runs out from the update period
                except NameError:
                    self.errorSignal.emit("Sonar not initialized.")
                except:
                    self.errorSignal.emit("Failed to read sonar.")
                self.updateSonarGuiSignal.emit("update") # Update the GUI
