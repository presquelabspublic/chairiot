# The PA6H (MKTK3339) GPS Module from Adafruit is detailed here: https://learn.adafruit.com/adafruit-ultimate-gps-on-the-raspberry-pi
# This library mostly makes use of the already-written GPSD library
# See wiring diagram here: https://learn.adafruit.com/adafruit-ultimate-gps-on-the-raspberry-pi/using-uart-instead-of-usb

import os
import serial
import time
import gps #GPSD Library

class pa6h():

    def __init__(self):
        # GPS is on Serial 0. For Raspberry Pi 3 make sure you also
        # 1. Turn on serial Hardware in raspi-config
        # 2. Put enable_uart=1 in the /boot/config.txt
        # 3. Remove any console=... from /boot/cmdline.txt EXCEPT console=tty1
        # 4. Run sudo chmod 777 /dev/serial0
        # Test code by connecting Pi TX and RX to each other
        # self.ser = serial.Serial("dev/serial0", timeout=0.1)

        # This line of code activates the GPSD library and opens a socket connection port
        os.system('sudo gpsd /dev/serial0 -F /var/run/gpsd.sock')
        self.session = gps.gps("localhost", "2947") #GPSD is on Port 2947

    def read_gps(self):

        # Test code by connecting Pi TX and RX to each other
        # self.ser.write("RUBEN")
        # time.sleep(0.1)
        # data = ser.read(10)

        self.session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)
        while True:
            try:
                report = self.session.next()
                # Wait for a 'TPV' report and display the current time
                # print report

                # If has TPV, report latitude and longitude
                if report['class'] == 'TPV':
                    if hasattr(report, 'lat') and hasattr(report, 'lon'):
                        return [report.lat, report.lon]
            except KeyError:
                pass
            except KeyboardInterrupt:
                quit()
            except StopIteration:
                    session = None
                    print ("GPSD has terminated")
