<?php

// This is a PHP script that is hosted on the server that allows acts as a Dashboard to visualize the data that it receives

// This function creates a connection to the SQL database
function Connection(){
  // Replace these four values with the specific setup that you have
  $server="localhost"; //Server Name
  $user="username"; //User Name
  $pass="password"; //Password
  $db="database_name"; //Database Name

  // Creates a connection
  $connection = mysql_connect($server, $user, $pass);

  // If the connection fails, call an error
  if (!$connection) {
    die('MySQL ERROR: ' . mysql_error());
  }

  // Select the SQL database, or failing that, call an error
  mysql_select_db($db) or die( 'MySQL ERROR: '. mysql_error() );

  // Return the connection
  return $connection;
}

// Call the function we just made
$link=Connection();

// Run the SQL query to get the data, store the data in $result
$result=mysql_query("SELECT * FROM `cartLog` ORDER BY `TIMESTAMP` DESC",$link);
?>


<html>
<!-- This is HTML code to view the data -->
<head>
  <!-- OPENLAYERS is a free Maps library -->
  <link rel="stylesheet"
  href="https://openlayers.org/en/v4.6.5/css/ol.css"
  type="text/css">

  <script src="https://openlayers.org/en/v4.6.5/build/ol.js"
  type="text/javascript">
  </script>

  <!-- Underscore JS is a utility-belt library fo JS that provides support for JS without extending any core JS objects -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"
  type="text/javascript">
  </script>

  <title>Cart Data</title>
  <!-- Refresh every 300 seconds -->
  <meta http-equiv="refresh" content="300">
</head>

<style>
/* This is a CSS Stylesheet */

/* Map boundaries */
.map {
  height: 400px;
  width: 100%;
}
</style>

<!-- Create the table that shows information -->
<body>
  <table border="1" cellspacing="1" cellpadding="1">
    <tr>
      <td>&nbsp;TIMESTAMP&nbsp;</td>
      <td>&nbsp;ID&nbsp;</td>
      <td>&nbsp;LATITUDE&nbsp;</td>
      <td>&nbsp;LONGITUDE&nbsp;</td>
      <td>&nbsp;BATT_VOLTAGE&nbsp;</td>
      <td>&nbsp;BATT_CURRENT&nbsp;</td>
      <td>&nbsp;BATT_TEMP&nbsp;</td>
      <td>&nbsp;CAVITY_TEMP&nbsp;</td>
      <td>&nbsp;MASS&nbsp;</td>
      <td>&nbsp;RFID&nbsp;</td>
      <td>&nbsp;SONAR&nbsp;</td>
      <td>&nbsp;FSR&nbsp;</td>
    </tr>

    <?php
    // This is a PHP script that creates table rows automatically. It takes the result from the above PHP script and creates a row for every entry
    if($result!==FALSE){
      $lat = array(); // Store latitude in a separate array
      $long = array(); // Store longitude in a separate array

      // Create the table with the results from the SQL query
      while($row = mysql_fetch_array($result)) {
        printf("<tr>
        <td> &nbsp;%s</td>
        <td> &nbsp;%s&nbsp; </td><td> &nbsp;%s&nbsp; </td>
        <td> &nbsp;%s&nbsp; </td><td> &nbsp;%s&nbsp; </td>
        <td> &nbsp;%s&nbsp; </td><td> &nbsp;%s&nbsp; </td>
        <td> &nbsp;%s&nbsp; </td><td> &nbsp;%s&nbsp; </td>
        <td> &nbsp;%s&nbsp; </td><td> &nbsp;%s&nbsp; </td>
        <td> &nbsp;%s&nbsp; </td>
        </tr>",
        $row["TIMESTAMP"], $row["ID"], $row["LATITUDE"], $row["LONGITUDE"],
        $row["BATT_VOLTAGE"], $row["BATT_CURRENT"], $row["BATT_TEMP"],
        $row["CAVITY_TEMP"], $row["MASS"], $row["RFID"], $row["SONAR"],
        $row["FSR"]);
      }

      mysql_free_result($result);
      mysql_close();
    }
    ?>
  </table>

  <!-- This is code to create the OpenLayers map -->
  <h2>Chairiot Locations</h2>

  <div id="map" class="map"></div>

  <script type="text/javascript">
  jQuery.noConflict();
  jQuery(document).ready(function() {

    // This Javascript function runs PHP code to get SQL data. We must do this in PHP because HTML cannot interact with SQL databases but PHP can.
    var data = <?php
    $server="localhost"; //Server Name
    $user="username"; //User Name
    $pass="password"; //Password
    $db="database_name"; //Database Name

    $connection = mysql_connect($server, $user, $pass);
    mysql_select_db($db);

    // This SQL query finds the most recent Latitude and Longitude for every ID
    $locations = mysql_query("SELECT `ID`, `LATITUDE`, `LONGITUDE`, MIN(`TIMESTAMP`) FROM `cartLog` GROUP BY `ID`", $connection);

    // Create an array called results amd put the $locations in these rows
    $rows = array();
    while($r=mysql_fetch_assoc($locations)) {
      $rows[] = $r;
    }

    // Return the results from the PHP SQL query in JSON format
    echo json_encode($rows);
    ?>;

    // Put data in array
    dataArray = _(data).toArray();
    console.log(dataArray);

    // Define initial location (from airport at Logan)
    // Note: LONGITUDE first
    var logan = ol.proj.fromLonLat([-71.02544, 42.36431]);

    // Define the view and center it, zoom it
    var view = new ol.View({
      center: logan,
      zoom: 15 // This is all the way out
    });

    // Create the map and set the target HTML
    var map = new ol.Map({
      target: "map"
    });

    // Initialize the markers we want to show
    var markers = [];

    for (i=0; i<dataArray.length; i++) {
      LAT = parseFloat(dataArray[i].LATITUDE); // Get the latitude from our latitude array
      LONG = parseFloat(dataArray[i].LONGITUDE); // Get the longitude from our longitude array

      // Create a marker at that latitude and longitude
      var marker = new ol.Feature({
        // Note: LONGITUDE first
        geometry: new ol.geom.Point(ol.proj.fromLonLat([LONG, LAT])),
        name: dataArray[i].ID
      });
      markers.push(marker);
    }

    // Vector Source for Map
    var vectorSource = new ol.source.Vector({
      features: markers
    });

    // Set an Icon for the map pins
    var iconStyle = new ol.style.Style({
      image: new ol.style.Icon({
        anchor: [0.5, 46],
        anchorXUnits: "fraction",
        anchorYUnits: "pixels",
        opacity: 0.75,
        src:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Map_marker_font_awesome.svg/32px-Map_marker_font_awesome.svg.png"
      })
    });

    // Vector Layer for Map
    var vectorLayer = new ol.layer.Vector({
      source: vectorSource,
      style: iconStyle
    });

    // Declare a Tile layer with Open Street Maps as the source
    var osmLayer = new ol.layer.Tile({
      source: new ol.source.OSM()
    });

    //Add the layer, set the view
    map.addLayer(osmLayer);
    map.addLayer(vectorLayer);
    map.setView(view);
  });

  </script>

</body>

</html>
