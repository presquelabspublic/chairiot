# This script POSTS data to the server via PHP

import sys
import config
import globals
import requests

class serverCom():

    def __init__(self):
        print("Initializing server connection... \n")

    def postDataClass(self, inDataClass, index):
        # This class takes in a dataClass and sends the data to the server
        outData = {
            "ID": inDataClass.id[index],
            "LATITUDE": inDataClass.latitude[index],
            "LONGITUDE": inDataClass.longitude[index],
            "BATT_VOLTAGE": inDataClass.battVoltage[index],
            "BATT_CURRENT": inDataClass.battCurrent[index],
            "BATT_TEMP": inDataClass.battTemp[index],
            "CAVITY_TEMP": inDataClass.cavityTemp[index],
            "MASS": inDataClass.load[index],
            "RFID": inDataClass.rfid[index],
            "SONAR": inDataClass.sonar[index],
            "FSR": inDataClass.forcedEntryDetected[index]
        }

        # Call to server
        postResponse = requests.post(
            url=config.SERVER_URL,
            data=outData
        )

        # Uncomment this line to see server response
        # print(postResponse)
