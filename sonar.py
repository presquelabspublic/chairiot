# This class activates and reads the sonar

import sys
import time
import config
import RPi.GPIO as GPIO

class sonar():

    def __init__(self):
        # Set GPIO modes
        GPIO.setmode(GPIO.BCM)
        # Setup the trigger and echo pins
        GPIO.setup(config.SONAR_TRIG, GPIO.OUT)
        GPIO.setup(config.SONAR_ECHO, GPIO.IN)

        GPIO.output(config.SONAR_TRIG, False)

    def measure(self):
        time.sleep(.1)  # Wait for sonar to settle
        GPIO.output(config.SONAR_TRIG, True)    # Send a high pulse for a duration
        time.sleep(0.00001) #in seconds
        GPIO.output(config.SONAR_TRIG, False)   # Send a low pulse

        while GPIO.input(config.SONAR_ECHO) == 0:
            pulse_start = time.time() # Pulse start

        while GPIO.input(config.SONAR_ECHO) == 1:
            pulse_end = time.time() # Pulse end

        pulse_duration = pulse_end - pulse_start

        #Distance in inches
        return round(pulse_duration * 6751.9685, 2)
