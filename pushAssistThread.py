# This class opens the PushAssist window and turns on the camera. The camera runs on a separate thread so that it doesn't interfere with data acquisition.

from dataClass import dataClass
from dataTable import dataTable
from multiprocessing import Queue
from PyQt5 import QtGui, QtCore
from picamera.array import PiRGBArray   # Most functionality comes from PiCamera
from picamera import PiCamera           # Most functionality comes from PiCamera

import cv2                              # OpenCV is very important as well
import config
import globals
import time
import sys

# Sensor Classes

try:
    from camera import camera
except Exception as e:
    print(e)

class pushAssistThread(QtCore.QThread):
    errorSignal = QtCore.pyqtSignal(str)
    updateStreamSignal = QtCore.pyqtSignal(QtGui.QPixmap)

    def __init__(self):
        QtCore.QThread.__init__(self)
        try:
            self.camera = PiCamera() # Initialize the camera
            self.camera.resolution = (640,480) # Set resolution
            self.camera.framerate = 30 # Set framerate
            self.rawCapture = PiRGBArray(self.camera, size=(640,480)) # Capture RGB Array
            time.sleep(0.1) # Sleep
            self.cameraState = 1
        except:
            pass

        self.start()

    def run(self):
        print("Starting Camera Thread...")

        while 1:
            if (self.cameraState == 1):
                try:

                    for frame in self.camera.capture_continuous(self.rawCapture, format="bgr", use_video_port=True):
                        # For every frame in the continuous capture,
                        image = frame.array # Pull an image out
                        height, width, channel = image.shape # Define its shape
                        bytesPerLine = 3 * width
                        qImg = QtGui.QImage(image.data, width, height, bytesPerLine, QtGui.QImage.Format_RGB888).rgbSwapped() # Make that image into a PyQt image
                        pixmap = QtGui.QPixmap.fromImage(qImg) # Create a Pixmap from image
                        self.updateStreamSignal.emit(pixmap) # Place the image in PyQt GUI
                        self.rawCapture.truncate(0) # I don't know what this does but this step is necessary. I think it clears the image
                        if self.cameraState == 0:
                            break
                except:
                    pass
            else:
                time.sleep(.1)

    @QtCore.pyqtSlot(int)
    def updateCameraState(self, update):
        self.cameraState = update
