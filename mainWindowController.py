# -*- coding: utf-8 -*-

################################################################################
##### IMPORT PYTHON CLASSES ####################################################
################################################################################
import sys

### QT imports ###
import PyQt5
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5.QtWidgets import *

### Misc Libs ###
import datetime
import time
import globals
import config
from daqThread import daqThread
from gpsThread import gpsThread
from rfidThread import rfidThread
from pushAssistThread import pushAssistThread
import threading
import logging

### Controller to create popup ###
import promptController
import errorsViewerController

### window from QT Creator generated with: "pyuic5 mainwindow.ui -x mainWindow.py" ###
import mainWindow

### Chairiot-written imports ###
try:
    from camera import camera
except Exception as e:
    print(e)

try:
    from servo import servo
except Exception as e:
    print(e)

try:
    import update
except Exception as e:
    print(e)

try:
    import audio
except Exception as e:
    print(e)

################################################################################
##### CLASS ####################################################################
################################################################################
class MainWindowController(QMainWindow, mainWindow.Ui_MainWindow):

    showingPrompt = False
    cameraStateSignal = QtCore.pyqtSignal(int)

    def __init__(self, app):
        super(self.__class__, self).__init__()
        # Defined in UI File
        self.setupUi(self)
        # Access variables inside of the UI's File
        self.app = app

        try:
            # Try moving the servo to lock position
            self.door = servo(config.SERVO_PIN, 0, 8, 32, "right")
        except Exception as e:
            self.handleError("Door lock failed to start.")

        # Set up initial state of GUI ##########################################
        self.timeLabel.setText(datetime.datetime.now().strftime("%H:%M"))
        self.batteryLabel.setText(str(globals.percentageBattery))
        self.batteryTempLabel.setText(str(globals.tempBattery))
        self.cabinTempLabel.setText(str(globals.tempCabin))
        self.loadLabel.setText(str(globals.weightLoad))
        self.passwordLabel.setText(config.PASSCODE_DEFAULT_TEXT);

        if (globals.lockState):
            self.lockDoor()     # See function lockDoor()
        else:
            self.unlockDoor()   # See function unlockDoor()

        try:
            update.update.update()  # Attempt Over-the-air update
        except Exception as e:
            self.handleError("Unable to perform over-the-air self-update.")

        # Set up Animations
        #self.percentageBatteryAnim = QtCore.QPropertyAnimation(self.batteryLabel, "color", self)
        #self.percentageBatteryAnim.setDuration(250)
        #self.percentageBatteryAnim.setLoopCount(2)

        # Set up listeners #####################################################

        # Login Screen #########################################################

        # Numpad Buttons
        self.onePushButton.clicked.connect(lambda:self.onePushButtonClicked())
        self.twoPushButton.clicked.connect(lambda:self.twoPushButtonClicked())
        self.threePushButton.clicked.connect(lambda:self.threePushButtonClicked())
        self.fourPushButton.clicked.connect(lambda:self.fourPushButtonClicked())
        self.fivePushButton.clicked.connect(lambda:self.fivePushButtonClicked())
        self.sixPushButton.clicked.connect(lambda:self.sixPushButtonClicked())
        self.sevenPushButton.clicked.connect(lambda:self.sevenPushButtonClicked())
        self.eightPushButton.clicked.connect(lambda:self.eightPushButtonClicked())
        self.ninePushButton.clicked.connect(lambda:self.ninePushButtonClicked())
        self.zeroPushButton.clicked.connect(lambda:self.zeroPushButtonClicked())

        # Clear and delete button
        self.deletePushButton.clicked.connect(lambda:self.deletePushButtonClicked())
        self.clearPushButton.clicked.connect(lambda:self.clearPushButtonClicked())

        # Unlock Door Button
        self.unlockPushButton.clicked.connect(lambda:self.unlockPushButtonClicked())

        # Main Menu ############################################################

        self.lockScreenPushButton.clicked.connect(lambda:self.lockScreenPushButtonClicked())
        self.historyPushButton.clicked.connect(lambda:self.historyPushButtonClicked())
        self.toggleDoorPushButton.clicked.connect(lambda:self.toggleDoorPushButtonClicked())
        self.pushAssistPushButton.clicked.connect(lambda:self.pushAssistPushButtonClicked())
        self.settingsPushButton.clicked.connect(lambda:self.settingsPushButtonClicked())

        # Settings Menus #######################################################

        self.settingsHomePushButton.clicked.connect(lambda:self.settingsHomePushButtonClicked())

        # History Menu #########################################################

        self.historyHomePushButton.clicked.connect(lambda:self.historyHomePushButtonClicked())

        # Push Assist Menu #####################################################

        self.pushAssistHomePushButton.clicked.connect(lambda:self.pushAssistHomePushButtonClicked())

        # Error Button

        self.errorsPushButton.clicked.connect(lambda:self.errorsPushButtonClicked())

        # Start DAQ thread and warning thread
        self.daqThread = daqThread()
        self.gpsThread = gpsThread()
        self.rfidThread = rfidThread()
        self.connectDaqThreadSignals()
        self.connectGpsThreadSignals()
        self.connectRfidThreadSignals()

    ############################################################################
    ##### Functions Called By the Listeners  ###################################
    ############################################################################

    ### Login Listener Functions  #####################################################################
    # Numpad Buttons
    def onePushButtonClicked(self):
        if (self.passwordLabel.text() == config.PASSCODE_DEFAULT_TEXT):
            self.passwordLabel.setText("")
        self.passwordLabel.setText(self.passwordLabel.text() + "1")

    def twoPushButtonClicked(self):
        if (self.passwordLabel.text() == config.PASSCODE_DEFAULT_TEXT):
            self.passwordLabel.setText("")
        self.passwordLabel.setText(self.passwordLabel.text() + "2")

    def threePushButtonClicked(self):
        if (self.passwordLabel.text() == config.PASSCODE_DEFAULT_TEXT):
            self.passwordLabel.setText("")
        self.passwordLabel.setText(self.passwordLabel.text() + "3")

    def fourPushButtonClicked(self):
        if (self.passwordLabel.text() == config.PASSCODE_DEFAULT_TEXT):
            self.passwordLabel.setText("")
        self.passwordLabel.setText(self.passwordLabel.text() + "4")

    def fivePushButtonClicked(self):
        if (self.passwordLabel.text() == config.PASSCODE_DEFAULT_TEXT):
            self.passwordLabel.setText("")
        self.passwordLabel.setText(self.passwordLabel.text() + "5")

    def sixPushButtonClicked(self):
        if (self.passwordLabel.text() == config.PASSCODE_DEFAULT_TEXT):
            self.passwordLabel.setText("")
        self.passwordLabel.setText(self.passwordLabel.text() + "6")

    def sevenPushButtonClicked(self):
        if (self.passwordLabel.text() == config.PASSCODE_DEFAULT_TEXT):
            self.passwordLabel.setText("")
        self.passwordLabel.setText(self.passwordLabel.text() + "7")

    def eightPushButtonClicked(self):
        if (self.passwordLabel.text() == config.PASSCODE_DEFAULT_TEXT):
            self.passwordLabel.setText("")
        self.passwordLabel.setText(self.passwordLabel.text() + "8")

    def ninePushButtonClicked(self):
        if (self.passwordLabel.text() == config.PASSCODE_DEFAULT_TEXT):
            self.passwordLabel.setText("")
        self.passwordLabel.setText(self.passwordLabel.text() + "9")

    def zeroPushButtonClicked(self):
        if (self.passwordLabel.text() == config.PASSCODE_DEFAULT_TEXT):
            self.passwordLabel.setText("")
        self.passwordLabel.setText(self.passwordLabel.text() + "0")

    #clear and delete Buttons
    def deletePushButtonClicked(self):
        if (self.passwordLabel.text() != config.PASSCODE_DEFAULT_TEXT):
            if (len(self.passwordLabel.text()) > 0):
                self.passwordLabel.setText(self.passwordLabel.text()[:-1])
            if (len(self.passwordLabel.text()) == 0):
                self.passwordLabel.setText(config.PASSCODE_DEFAULT_TEXT)

    def clearPushButtonClicked(self):
        self.passwordLabel.setText(config.PASSCODE_DEFAULT_TEXT)

    #unlock Screen push Button
    def unlockPushButtonClicked(self):
        if (self.passwordLabel.text() in config.PASSCODES):
            self.passwordLabel.setText(config.PASSCODE_DEFAULT_TEXT)
            self.centerStack.setCurrentIndex(1)
        else:
            self.wrongPasswordPrompt = promptController.promptController("Wrong Passcode")
            self.promptShow(self.wrongPasswordPrompt, config.PROMPT_DURATION)

    # Main Menu Screen Listener Functions ################################################

    def lockScreenPushButtonClicked(self):
        self.centerStack.setCurrentIndex(0)

    def historyPushButtonClicked(self):
        self.centerStack.setCurrentIndex(4)

    def toggleDoorPushButtonClicked(self):
        globals.lockState = not globals.lockState

        if (not globals.lockState):
            self.unlockDoor()
        else:
            self.lockDoor()


    def pushAssistPushButtonClicked(self):
        self.centerStack.setCurrentIndex(3)
        try:
            self.pushAssistThread
            self.cameraStateSignal.emit(1)
        except:
            self.pushAssistThread = pushAssistThread()
            self.connectPushAssistThreadSignals()

    def settingsPushButtonClicked(self):
        self.centerStack.setCurrentIndex(2)

    # History Menu Listner Functions ######################################################

    def historyHomePushButtonClicked(self):
        self.centerStack.setCurrentIndex(1)

    # Settings Menu Listener Functions ####################################################

    def settingsHomePushButtonClicked(self):
        self.centerStack.setCurrentIndex(1)

    # Push Assist Menu Listener Functions ###################################################

    def pushAssistHomePushButtonClicked(self):
        self.cameraStateSignal.emit(0)
        self.centerStack.setCurrentIndex(1)

    def errorsPushButtonClicked(self):
        self.errorsViewer = errorsViewerController.errorsViewerController(self.errorsPushButton)
        self.errorsViewer.show()
        globals.showingErrors = True

    # CONNECT SIGNALS ##########################################################

    def connectUpdateGuiSignal(self):
        self.daqThread.updateGuiSignal.connect(self.updateGui)

    def connectErrorHandleSignal(self):
        self.daqThread.errorSignal.connect(self.handleError)

    def connectDaqThreadSignals(self):
        self.daqThread.updateGuiSignal.connect(self.updateGui)
        self.daqThread.errorSignal.connect(self.handleError)
        self.daqThread.updateSonarGuiSignal.connect(self.updateSonarGui)

    def connectGpsThreadSignals(self):
        self.gpsThread.errorSignal.connect(self.handleError)

    def connectRfidThreadSignals(self):
        self.rfidThread.errorSignal.connect(self.handleError)
        self.rfidThread.cardScannedSignal.connect(self.rfidScanned)

    def connectPushAssistThreadSignals(self):
        self.pushAssistThread.errorSignal.connect(self.handleError)
        self.pushAssistThread.updateStreamSignal.connect(self.updatePushAssistStream)
        self.cameraStateSignal.connect(self.pushAssistThread.updateCameraState)

    # Signal Functions #########################################################

    @QtCore.pyqtSlot(str)
    def rfidScanned(self, message):
        # This function is called by the RFID thread whenever an RFID card is scanned. The RFID thread scans the ID and sets message to "granted"
        if (message == "granted"):
            if globals.lockState:
                audio.audio.play_audio_file("rfidAccepted.wav")
                self.unlockDoor()
                self.centerStack.setCurrentIndex(1)
            else:
                audio.audio.play_audio_file("rfidAccepted.wav")
                self.lockDoor()
                self.centerStack.setCurrentIndex(0)
        else:
            # If message is not "granted", then access denied
            self.accessDeniedPrompt = promptController.promptController("Access Denied")
            self.promptShow(self.accessDeniedPrompt, config.PROMPT_DURATION)

    @QtCore.pyqtSlot(str)
    def handleError(self, message):
        # This function is called whenever there is an error. It adds the error to the errorLog (a text file) and updates the error GUI button
        globals.unreadErrors = globals.unreadErrors + 1
        self.errorsPushButton.setText(config.ERRORS_BUTTON_TEXT + str(globals.unreadErrors))
        logging.basicConfig(filename='errorLog',
                            filemode='a',
                            format='%(asctime)s %(message)s',
                            datefmt='%Y%m%d_%H:%M:%S',
                            level=logging.DEBUG)
        logging.info(message)
        globals.errorText = globals.errorText + "\n" + message
        if globals.showingErrors:
            self.errorsViewer.updateText()

    @QtCore.pyqtSlot(str)

    def updateGui(self, message):
        # This function is called whenever there is need to update the GUI.

        ### Check for adverse states ###

        # If the door is open and the lock is in place and the door has been closed since the lock, show tamper message
        if (not globals.doorState and globals.lockState and globals.doorClosedSinceLock):
            self.tamperPrompt = promptController.promptController("Forced Entry Detected")
            self.promptShow(self.tamperPrompt, config.PROMPT_DURATION)
            globals.forcedEntryDetected = True
        else:
            globals.forcedEntryDetected = False

        # If the battery is below 25%, make the GUI battery icon red
        if globals.percentageBattery < 25:
            self.batteryLabel.setStyleSheet("QTextEdit{color:red}")

        # If the temperature of the battery is too low/high, throw an error
        if (globals.tempBattery < -40) or (globals.tempBattery > 140):
            self.handleError("Battery temperature too high or too low.")
            pass

        # If the temperature of the cabin is too low/high, throw an error
        if (globals.tempCabin < 14) or (globals.tempCabin > 100):
            self.handleError("Cabin temperature too high or too low.")
            pass

        # If there is too much weight on the cart, throw an error
        if globals.weightLoad > 750:
            self.handleError("Cart over specified weight capacity (750 lbs).")
            pass

        # If there is something within 2 feet of the cart, play an alarm
        if globals.distanceSonar < 24:
            self.handleError("Sonar proximity alert!")
            audio.audio.play_audio_file("proximityAlert.wav")
            pass

        self.timeLabel.setText(datetime.datetime.now().strftime("%H:%M")) # Update the date and time
        self.batteryLabel.setText(str(int(round(globals.percentageBattery))) + "%") # Update the battery percentage
        self.batteryTempLabel.setText(str(int(round(globals.tempBattery))) + "°F") # Update the battery temperature
        self.cabinTempLabel.setText(str(int(round(globals.tempCabin))) + "°F") # Update the cabin temperature
        self.loadLabel.setText(str(int(round(globals.weightLoad))) + " lbs") # Update the weight sensor
        self.sonarLabel.setText(str(int(round(globals.distanceSonar))) + "in") # Update the sonar

        if (globals.lockState):
            self.lockStateLabel.setText("Locked")
        else:
            self.lockStateLabel.setText("Unlocked")

    @QtCore.pyqtSlot(QtGui.QPixmap)
    def updatePushAssistStream(self, pixmap):
        # This function is called when the Push Assist button is pushed
        self.videoStreamLabel.setPixmap(pixmap)

    @QtCore.pyqtSlot(str)
    def updateSonarGui(self, message):
        # This function is called when the Push Assist is pushed
        self.sonarLabel.setText(str(int(round(globals.distanceSonar))) + "in")

    # Misc Functions #################################################################

    def promptShow(self, form, duration):
        # This function shows a prompt for some duration of time
        if not self.showingPrompt:
            startTime = time.time()
            self.showingPrompt = True
            form.show()
            threading.Timer(duration, self.promptClose, {form: form}).start()

    def promptClose(self, form):
        # This function closes the prompt
        form.close()
        self.showingPrompt = False

    def unlockDoor(self):
        # This function unlocks the door
        try:
            self.door.moveAngle(-80)
            globals.lockState = False
            self.lockStateLabel.setText("Unlocked")
            self.toggleDoorPushButton.setText("Lock Door")
            error = audio.audio.play_audio_file("rfidAccepted.wav")
            if error is not None:
                print(error)
        except Exception as e:
            print (e)

    def lockDoor(self):
        # This function locks the door
        try:
            self.door.moveAngle(80)
            globals.lockState = True
            if globals.doorState:
                globals.doorClosedSinceLock = True
            else:
                globals.doorClosedSinceLock = False
            self.lockStateLabel.setText("Locked")
            self.toggleDoorPushButton.setText("UnlockDoor")
            error = audio.audio.play_audio_file("rfidAccepted.wav")
            if error is not None:
                print(error)
        except Exception as e:
            print (e)
