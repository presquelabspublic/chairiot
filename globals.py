# This is a global class that contains all of the variables

import sys

def init():
    global doorState                # Is the door open or closed, T/F
    global distanceSonar            # Sonar distance measurement, in.
    global latitude                 # GPS Latitude, in EPSG:3857 Projection
    global longitude                # GPS Longitude, in EPSG:3857 Projection
    global percentageBattery        # Battery Percentage, %
    global voltageBattery           # Battery Voltage, V
    global currentBattery           # Battery Current, A
    global rfidReading              # rfidReading, text
    global tempBattery              # Battery Temperature, degrees F
    global tempCabin                # Cabin Temperature, degrees F
    global weightLoad               # Load, in lbs
    global lockState                # Is the door locked or unlocked, T/F
    global unreadErrors             # Number of unread errors
    global errorText                # Text for error GUI
    global showingErrors            # Are the errors showing
    global fsrReading               # Reading from the FSR
    global doorClosedSinceLock      # Has the door been closed since lock, T/F
    global id                       # Cart Identification
    global forcedEntryDetected      # Has a forced entry been detected, T/F

    # Initialize values
    doorState = True # True means closed, False means open
    lockState = False # True means locked, False means unlocked
    distanceSonar = 0
    latitude = 0
    longitude = 0
    voltageBattery = 0
    currentBattery = 0
    percentageBattery = 0
    rfidReading = 0
    tempBattery = 0
    tempCabin = 0
    weightLoad = 0
    unreadErrors = 0
    errorText = ""
    showingErrors = False

    # True means door has been closed since the lock and if opened again means tampering
    doorClosedSinceLock = False
    forcedEntryDetected = False

    # Get the Raspberry Pi ID
    id = "NONE"
    try:
        # Raspberry PI ID is in /proc/cpuinfo, we open it read-only
        for line in open('/proc/cpuinfo', 'r'):
            # Read through the lines. If the line contains the "Serial" tag, the ID should follow
            if line[0:6]=='Serial':
                id = line[10:26]
    except Exception as e:
        print(e)
        id = "UNIDENTIFIED ID AT LATITUDE " + str(latitude) + " AND LONGITUDE " + str(longitude)
