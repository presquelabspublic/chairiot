<?php

// This is a PHP script that you place on your server to LISTEN for incoming data and then place that data inside a SQL database that exists on the same server

// This function creates a connection to the SQL database.
function Connection(){
  // Replace these next four values with the specific setup that you have.
  $server="localhost"; //Server Name
  $user="username"; //User Name
  $pass="password"; //Password
  $db="database_name"; //Database Name

  // Creates a connection
  $connection = mysql_connect($server, $user, $pass);

  // If the connection fails, call an error.
  if (!$connection) {
      die('MySQL ERROR: ' . mysql_error());
  }

  // Select the correct SQL database, or failing that, throw an error.
  mysql_select_db($db) or die( 'MySQL ERROR: '. mysql_error() );

  // Return the SQL connection
  return $connection;
}

  // We call the function Connection() that we just defined.
  $link=Connection();

  // POST the values that we are listening for to the SQL database.
  $ID=$_POST["ID"];
	$LATITUDE=$_POST["LATITUDE"];
  $LONGITUDE=$_POST["LONGITUDE"];
  $BATT_VOLTAGE=$_POST["BATT_VOLTAGE"];
  $BATT_CURRENT=$_POST["BATT_CURRENT"];
  $BATT_TEMP=$_POST["BATT_TEMP"];
  $CAVITY_TEMP=$_POST["CAVITY_TEMP"];
  $MASS=$_POST["MASS"];
  $RFID=$_POST["RFID"];
  $SONAR=$_POST["SONAR"];
  $FSR=$_POST["FSR"];

  // This is the actual query (an INSERT query) that puts the values in the SQL table
	$query = "INSERT INTO `cartLog` (`ID`, `LATITUDE`, `LONGITUDE`,
            `BATT_VOLTAGE`, `BATT_CURRENT`, `BATT_TEMP`, `CAVITY_TEMP`, `MASS`,
            `RFID`, `SONAR`, `FSR`)
            VALUES ('".$ID."','".$LATITUDE."','".$LONGITUDE."',
            '".$BATT_VOLTAGE."','".$BATT_CURRENT."','".$BATT_TEMP."',
            '".$CAVITY_TEMP."','".$MASS."','".$RFID."','".$SONAR."','".$FSR."')";

  // Run the query with the connection
  mysql_query($query,$link);

  // Close the connection.
  mysql_close($link);

  header("Location: index.php");
?>
