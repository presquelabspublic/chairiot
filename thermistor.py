# Temperature sensor class.
#See wiring diagram here: http://www.circuitbasics.com/wp-content/uploads/2016/03/Raspberry-Pi-DS18B20.png

import os
import glob #Filename pattern matching
import time



class thermistor():

    def __init__(self, whichone):
        #Initialize the GPIO pins
        os.system('modprobe w1-gpio') #Turns on the GPIO Module
        os.system('modprobe w1-therm') #Turns on the thermistor Module

        #Finds the base directory that holds temperature data
        base_dir = '/sys/bus/w1/devices/'
        #All thermistors start with 28 - we choose one using glob
        device_folder = glob.glob(base_dir + '28*')[whichone]
        self.device_file = device_folder + '/w1_slave'

    def read_temp_raw(self):
        #Open (read-only) data file with temperature in it. open is from os
        f = open(self.device_file, 'r')
        #Read the lines as a string
        lines = f.readlines()
        #Close the file we just opened
        f.close

        return lines
        #lines should look like this:
        #bd 01 4b 46 7f ff 03 10 ff : crc=ff YES
        #bd 01 4b 46 7f ff 03 10 ff t=27812

        #If there is a YES, then a temperature has been recorded. If NO, the
        #second line will not exist

    def read_temp(self):
        #Get temperature data
        lines = self.read_temp_raw()

        #We look at the last three letters of the first line. If not YES, wait
        #and check again after sleep
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = self.read_temp_raw()

        #If YES, find t
        equals_pos = lines[1].find('t=')

        #If t exists, find the temperature
        if equals_pos != -1:
            temp_string = lines[1][equals_pos+2:]
            temp_c = float(temp_string) / 1000.0
            temp_f = temp_c * 9.0 / 5.0 + 32.0
            return temp_c, temp_f
