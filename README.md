# chairiot

Chairiot’s mission is to bring the Airport Material Tranport Cart (AMTC) into the next generation.

The fundamental purpose of AMTCs is to move goods from one place to another. Current AMTCs are plain and old-fashioned – relics of the past. We aim to modernize the challenge of moving goods by incorporating technology that makes our lives easier and more efficient. Applying up-to-date engineering methods and techniques makes Chairiot that much safer, faster, and smarter than its older siblings.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

* [Python](https://www.python.org)
* [Raspberry Pi](https://www.raspberrypi.org/)

### Installing

#### Raspberry Pi Setup

Please note that this Python code is VERY LIMITED on Windows.

There are a couple of things you want to enable on your Raspberry Pi by running the following commands.

First, enable the following in the Raspberry Pi Configuration. You can do this either by running ```sudo raspi-config``` or by clicking the raspberry and selecting Configuration.

* I2C
* SSH
* SPI
* VNC
* WIFI

Next, you want to run the following commands in the Terminal
```
git config credential.helper store
git clone https://eugeneyng@bitbucket.org/presquelabsaero/chairiot.git
cd chairiot
```

##### Raspberry Pi SHUTDOWN
Please note that to safely shutdown the Raspberry Pi, the following command should be used:

` sudo shutdown -h now `

This prevents

#### Python Environment Setup
If you have [Anaconda](https://conda.io/miniconda.html) (Miniconda is recommended because of the size of Anaconda), you can run `conda env create --file=environment.yml` and that should take care of your Python environment for you. If not, you will have to set up your Python environment manually by running the following commands. You will also have to change several configuration files on the Raspberry Pi, separate from Python.

* AUDIO Setup:
  * ` sudo nano /etc/asound.conf `
  * Add the following lines:
    * ` pcm.!default { type hw card 1 } `
    * ` ctl.!default { type hw card 1 } `

* ADC Setup:
  * ` cd Adafruit_Python_MCP3008 `
  * ` sudo python setup.py install `

* CAMERA Setup:
  * `sudo pip install picamera`

* DS18B20 Temperature Sensor Setup:
  * `sudo nano /boot/config.txt`
  * Add `dtoverlay=w1-gpio,gpiopin=16` to the end of the file

* GUI Setup:
  * `sudo apt-get install python-pyqt5`

* GPS Setup:
  * `sudo apt-get install gpsd gpsd-clients python-gps`
  * `sudo systemctl stop gpsd.socket`
  * `sudo systemctl disable gpsd.socket`
  * `sudo nano /boot/cmdline.txt`
    * remove console=ttyAMA0,115200 OR console=serial0,115200 OR console=ttyS0,115200 and if there, kgdboc=ttyAMA0,115200
    * Really just remove any `console=` EXCEPT `console=tty1`
  * `sudo nano /boot/config.txt`
    * add `enable_uart=1` to the bottom of the file
  * `sudo killall gpsd`
  * `sudo gpsd /dev/ttyS0 -F /var/run/gpsd.sock`
  * `sudo chmod 777 /dev/serial0`

* GPIO Setup:
  * `cd Adafruit_Python_GPIO`
  * `sudo python setup.py install`

* HX711 Strain Gauge Amplifier Setup:
  * `sudo apt-get python-numpy`

* RFID Setup:
  * `cd Adafruit_Python_PN532`
  * `sudo python setup.py install`

* Servo Setup:
  * `sudo pip install pigpio`
  * `sudo systemctl enable pigpiod`

* TMP120 Temperature Sensor Setup:
  * `sudo apt-get install lm-sensors`
  * `sudo apt-get install i2c-tools`

## Deployment

Run `python startup.py` in your chosen directory.

## Built With

* [Atom](https://atom.io/) - Text editor of choice
* [Bitbucket](https://bitbucket.org/) - Git management of choice

## Contributing

Contribution to Chairiot is currently closed.

## Versioning

We use [Bitbucket](https://bitbucket.org/) for Git control.
We do not currently have version tags.

## Authors

* **Eric Simmons**
* **Eugene Ng**
* **Ruben Ghijsen**
* **Mark Henry**

## License

Copyright (C) Presque Labs LLC - All Rights Reserved
Unauthorized copying of this file, via any medium, is strictly prohibited.
Proprietary and Confidential.
2018

## Acknowledgments

* The GPIO, MCP3008, and PN532 libraries were provided by Adafruit.
