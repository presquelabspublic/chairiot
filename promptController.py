import sys

#Qt imports
import PyQt5
from PyQt5 import QtCore
from PyQt5.QtWidgets import *

import datetime
import time
import globals
import config

# window from QT Creator generated with: "pyuic5 mainwindow.ui -x mainWindow.py"
import prompt

#create class for the GUI
class promptController(QDialog, prompt.Ui_prompt):

	#access variables inside of the UI's File
    def __init__(self, promptText):
        super(self.__class__, self).__init__()
        #defined in UI File
        self.setupUi(self)
        self.promptText.setText(promptText)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
