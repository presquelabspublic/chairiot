import os

class update():

    # No need to create an instance of the class. Just call this function from wherever, as long as you import it.
    @staticmethod
    def update():
        try:
            os.system("cd ~/chairiot") # go to Chairiot directory
            os.system("git remote add origin https://gitlab.com/presquelabs/chairiot.git") # Add origin
            os.system("git fetch origin") # Fetch origin
            os.system("git reset --hard origin/master") # Reset all code
            os.system("git pull") # Pull
        except Exception as e:
            return e
